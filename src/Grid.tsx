import React from 'react';
import './Grid.css';

type GridProps = {
    cells: number;
    cols: number;
    highlighted: number[];
    onCellClick: (cell: number) => void;
    readonly?: boolean;
    size: "normal" | "nano" | "mini"
}

function Grid({cells, cols, highlighted, onCellClick, readonly, size}: GridProps) {
    return (
        <div className="GridContainer">
            <div className={"Grid " + size + (readonly === true ? " readonly" : "")}>
                {Array.from({length: Math.floor(cells / cols) + (cells % cols > 0 ? 1 : 0)}, (_, rowIndex) => (
                    <div key={rowIndex} className="Row">
                        {Array.from({length: rowIndex === Math.floor(cells / cols) ? (cells % cols) : cols}, (_, colIndex) => (
                            <div key={cols * rowIndex + colIndex}
                                 className={"Cell" + (highlighted.includes(cols * rowIndex + colIndex) ? " CellHighlight" : "")}
                                 onClick={() => onCellClick(cols * rowIndex + colIndex)}/>
                        ))}
                    </div>
                ))}
            </div>
        </div>
    );
}

export default Grid;
