import React, {useEffect, useMemo, useState} from 'react';
import {useTranslation} from "react-i18next";
import BlockerOverlay from "./BlockerOverlay";
import Grid from "./Grid";
import {StatLine, TestSet} from "./types";

import './TestPanel.css';

type Phase = "PRESENTATION" | "RECALL" | "STATS";

const initialDelay = 500;
const displayDuration = 3000;
const waitDuration = 300;
const casesInSet = 3;

type TestPanelProps = {
    tests: TestSet[];
    onFinish: (stats: StatLine[]) => void;
};

function TestPanel({tests, onFinish}: TestPanelProps) {
    const {t} = useTranslation();

    const [solvedInSet, setSolvedInSet] = useState(0);
    const [testSet, setTestSet] = useState(0);
    const [testCase, setTestCase] = useState(1);

    const pattern = useMemo<number[]>(() => tests[testSet][testCase - 1].highlighted, [tests, testSet, testCase]);
    const [recallPattern, setRecallPattern] = useState<number[]>([]);

    const [phase, setPhase] = useState<Phase>("PRESENTATION");
    const [highlighted, setHighlighted] = useState<number[]>([]);

    const [stats, setStats] = useState<StatLine[]>([]);

    useEffect(() => {
        if (phase !== "PRESENTATION") return;

        setHighlighted([]);
        let timeoutHandle: any = setTimeout(() => {
            setHighlighted(pattern);
            timeoutHandle = setTimeout(() => {
                setHighlighted([]);
                timeoutHandle = setTimeout(() => {
                    timeoutHandle = undefined;
                    setRecallPattern([]);
                    setPhase("RECALL");
                }, waitDuration);
            }, displayDuration);
        }, initialDelay);

        return () => {
            if (timeoutHandle) clearTimeout(timeoutHandle);
        };
    }, [pattern, phase]);

    const handleCellClick = (cellNo: number) => {
        if (phase !== "RECALL") return;

        const index = recallPattern.indexOf(cellNo);
        if (index === -1) {
            const newRecallPattern = [...recallPattern, cellNo].sort((a, b) => a - b);
            setRecallPattern(newRecallPattern);
            setHighlighted(newRecallPattern);
        } else {
            const newRecallPattern = [...recallPattern.slice(0, index), ...recallPattern.slice(index + 1)];
            setRecallPattern(newRecallPattern);
            setHighlighted(newRecallPattern);
        }
    };

    const handleDone = () => {
        if (phase !== "RECALL") return;
        if (recallPattern.length < pattern.length) return;

        setHighlighted([]);

        const lastScore = recallPattern.filter(cellNo => pattern.includes(cellNo)).length;
        const lastMaxScore = pattern.length;

        setStats(scores => [...scores, {
            testCase: tests[testSet][testCase - 1],
            caseNo: testCase,
            recallPattern,
            score: lastScore
        }]);

        const newSolvedCount = lastScore === lastMaxScore ? solvedInSet + 1 : solvedInSet;
        if (testCase === casesInSet) {
            if (newSolvedCount < 2) {
                setPhase("STATS")
                return;
            } else if (testSet === tests.length - 1) {
                setPhase("STATS");
                return;
            } else {
                setSolvedInSet(0);
                setTestSet(testSet => testSet + 1);
                setTestCase(1);
                setPhase("PRESENTATION");
            }
        } else {
            setSolvedInSet(newSolvedCount);
            setTestCase(testCase => testCase + 1);
            setPhase("PRESENTATION");
        }
    }

    return (
        <div className="TestPanel">
            {phase !== "STATS" && (
                <Grid
                    cells={2 * tests[testSet][testCase - 1].difficulty}
                    cols={tests[testSet][testCase - 1].columns}
                    highlighted={highlighted}
                    onCellClick={handleCellClick}
                    size="normal"
                />
            )}

            <form className="Controls">
                {phase === "RECALL" &&
                    <button type="button" onClick={handleDone}
                            disabled={recallPattern.length !== pattern.length}>{t("basic.done")}</button>}

                {phase === "STATS" && <button type="button" onClick={() => onFinish(stats)}>{t("basic.end")}</button>}
            </form>

            {phase === "PRESENTATION" && <BlockerOverlay/>}

            {phase === "STATS" && (
                <details className="Stats">
                    <summary>{t("stats.summary")}</summary>
                    <table className="StatsTable">
                        <thead>
                        <tr>
                            <th>{t("stats.level")}</th>
                            <th>{t("stats.caseNo")}</th>
                            <th>{t("stats.score")}</th>
                            <th>{t("stats.pattern")}</th>
                            <th>{t("stats.guess")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {stats.map(({testCase, caseNo, recallPattern, score}, index) => (
                            <tr key={index}>
                                <td>{testCase.difficulty}</td>
                                <td>{caseNo}</td>
                                <td>{score}</td>
                                <td>
                                    <Grid cells={2 * testCase.difficulty} cols={testCase.columns}
                                          highlighted={testCase.highlighted} onCellClick={() => undefined} size="nano"/>
                                </td>
                                <td>
                                    <Grid cells={2 * testCase.difficulty} cols={testCase.columns}
                                          highlighted={recallPattern} onCellClick={() => undefined} size="nano"/>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </details>
            )}

        </div>
    );
}

export default TestPanel;
