import React, {useEffect, useMemo, useState} from 'react';
import {useTranslation} from "react-i18next";
import BlockerOverlay from "./BlockerOverlay";
import Grid from "./Grid";
import {TestCase} from "./types";

import './IntroPanel.css';

const initialDelay = 500;
const displayDuration = 3000;
const waitDuration = 300;

type Step =
    | { type: "text"; text: string; buttonText: string; }
    | { type: "presentation"; testCase: TestCase; }
    | { type: "test"; testCase: TestCase; };

type Phase = "TEXT" | "PRESENTATION" | "RECALL" | "PASS" | "FAIL";

type IntroPanelProps = {
    onFinish: () => void;
}

function IntroPanel({onFinish}: IntroPanelProps) {
    const {t} = useTranslation();

    const introSteps: Step[] = useMemo(() => [
        {
            type: "text",
            text: t("intro.texts.0"),
            buttonText: t("intro.button.next"),
        },
        {
            type: "presentation",
            testCase: {
                difficulty: 2,
                columns: 2,
                highlighted: [0, 3]
            },
        },
        {
            type: "text",
            text: t("intro.texts.1"),
            buttonText: t("intro.button.startTrial"),
        },
        {
            type: "test",
            testCase: {
                difficulty: 2,
                columns: 2,
                highlighted: [0, 3]
            },
        },
        {
            type: "test",
            testCase: {
                difficulty: 2,
                columns: 2,
                highlighted: [0, 1]
            },
        },
        {
            type: "test",
            testCase: {
                difficulty: 3,
                columns: 3,
                highlighted: [0, 4, 5],
            },
        },
        {
            type: "test",
            testCase: {
                difficulty: 3,
                columns: 3,
                highlighted: [1, 2, 3],
            },
        },
        {
            type: "text",
            text: t("intro.texts.2"),
            buttonText: t("intro.button.next"),
        },
        {
            type: "text",
            text: t("intro.texts.3"),
            buttonText: t("intro.button.next"),
        },
    ], [t]);

    const [stepIndex, setStepIndex] = useState(0);
    const currentStep = useMemo(() => introSteps[stepIndex], [introSteps, stepIndex]);

    const [phase, setPhase] = useState<Phase>(() => currentStep.type === "text" ? "TEXT" : "PRESENTATION");

    const [recallPattern, setRecallPattern] = useState<number[]>([]);
    const [highlighted, setHighlighted] = useState<number[]>([]);

    useEffect(() => {
        if (currentStep.type === "text" || phase !== "PRESENTATION") return;

        setHighlighted([]);
        let timeoutHandle: any = setTimeout(() => {
            setHighlighted(currentStep.testCase.highlighted);
            timeoutHandle = setTimeout(() => {
                setHighlighted([]);
                timeoutHandle = setTimeout(() => {
                    timeoutHandle = undefined;
                    setRecallPattern([]);
                    setPhase("RECALL");
                }, waitDuration);
            }, displayDuration);
        }, initialDelay);

        return () => {
            if (timeoutHandle) clearTimeout(timeoutHandle);
        };
    }, [currentStep, phase]);

    const handleCellClick = (cellNo: number) => {
        if (currentStep.type !== "test" || phase !== "RECALL") return;

        const index = recallPattern.indexOf(cellNo);
        if (index === -1) {
            const newRecallPattern = [...recallPattern, cellNo].sort((a, b) => a - b);
            setRecallPattern(newRecallPattern);
            setHighlighted(newRecallPattern);
        } else {
            const newRecallPattern = [...recallPattern.slice(0, index), ...recallPattern.slice(index + 1)];
            setRecallPattern(newRecallPattern);
            setHighlighted(newRecallPattern);
        }
    };

    const handleDone = () => {
        if (currentStep.type !== "test" || phase !== "RECALL") return;

        const pattern = currentStep.testCase.highlighted;
        if (recallPattern.length < pattern.length) return;

        setHighlighted([]);

        const score = recallPattern.filter(cellNo => pattern.includes(cellNo)).length;
        const maxScore = pattern.length;

        if (score === maxScore) {
            setPhase("PASS")
        } else {
            setPhase("FAIL")
        }
    }

    const handleNext = () => {
        const nextStepIndex = stepIndex + 1;
        if (nextStepIndex === introSteps.length) {
            onFinish();
        } else {
            setStepIndex(nextStepIndex);
            setPhase(() => introSteps[nextStepIndex].type === "text" ? "TEXT" : "PRESENTATION")
        }
    };

    const handleBack = () => {
        setPhase("PRESENTATION");
    };

    return (
        <div className="IntroPanel">
            {currentStep.type === "text" &&
                <span className="Text">{currentStep.text}</span>}

            {(currentStep.type === "test" && phase === "PASS") &&
                <span className="Text">{t("intro.feedback.pass")}</span>}

            {(currentStep.type === "test" && phase === "FAIL") &&
                <span className="Text">{t("intro.feedback.fail")}</span>}

            {(currentStep.type !== "text" && phase !== "PASS" && phase !== "FAIL") && (
                <Grid
                    cells={2 * currentStep.testCase.difficulty}
                    cols={currentStep.testCase.columns}
                    highlighted={highlighted}
                    onCellClick={handleCellClick}
                    readonly={currentStep.type === "presentation"}
                    size="normal"
                />
            )}

            <form className="Controls">
                {(currentStep.type === "text" && phase === "TEXT") &&
                    <button type="button" onClick={handleNext}>{currentStep.buttonText}</button>}

                {(currentStep.type === "presentation" && phase === "RECALL") &&
                    <button type="button" onClick={handleNext}>{t("basic.next")}</button>}

                {(currentStep.type === "test" && phase === "RECALL") &&
                    <button type="button" onClick={handleDone}
                            disabled={currentStep.type === "test" &&
                                recallPattern.length !== currentStep.testCase.highlighted.length}>{t("basic.done")}</button>}

                {(currentStep.type === "test" && phase === "PASS") &&
                    <button type="button" onClick={handleNext}>{t("basic.next")}</button>}

                {(currentStep.type === "test" && phase === "FAIL") &&
                    <button type="button" onClick={handleBack}>{t("basic.back")}</button>}
            </form>

            {(currentStep.type === "test" && phase === "PRESENTATION") && <BlockerOverlay/>}
        </div>
    );
}

export default IntroPanel;
