import React, {useEffect, useMemo, useState} from 'react';
import {useTranslation} from "react-i18next";
import {TestSet} from "./types";

import './LoadPanel.css';

type LoadPanelProps = {
    tests: TestSet[];
    onLoad: (tests: TestSet[]) => void;
}

function LoadPanel({tests: initialTests, onLoad}: LoadPanelProps) {
    const {t} = useTranslation();
    const [tests, setTests] = useState(initialTests);

    const [data, setData] = useState(btoa(JSON.stringify(tests)))

    useEffect(() => {
        try {
            setTests(JSON.parse(atob(data)))
        } catch (e) {
            setTests([]);
        }

    }, [data]);

    const isValid = useMemo(() => tests.length > 0 && tests.every(testSet => testSet.every(pattern => pattern.difficulty === pattern.highlighted.length)), [tests]);

    const handleLoad = () => {
        if (isValid) {
            onLoad(tests);
        }
    }

    const handleReset = () => {
        setData(btoa(JSON.stringify(initialTests)));
    };

    return (
        <form className="LoadPanel">
            <button type="button" onClick={handleLoad} disabled={!isValid}>{t("editor.load")}</button>
            <textarea value={data} cols={80} rows={30}
                      className={"DataField " + (isValid ? "correct" : "error")}
                      onChange={e => setData(e.currentTarget.value)}
                      onFocus={e => e.currentTarget.select()}/>
            <button type="button" onClick={handleReset}>{t("editor.reset")}</button>
        </form>
    );
}

export default LoadPanel;
