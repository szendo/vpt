import React, {useCallback, useEffect, useState} from 'react';
import {useTranslation} from "react-i18next";
import EditPanel from "./EditPanel";
import IntroPanel from "./IntroPanel";
import LoadPanel from "./LoadPanel";
import MenuPanel from "./MenuPanel";
import ResultsPanel from "./ResultsPanel";
import TestPanel from "./TestPanel";
import {testSets} from "./testSets";
import {Results, StatLine, TestSet} from "./types";

import './App.css';

type Phase = "MENU" | "INTRO" | "TEST" | "RESULTS" | "EDIT" | "LOAD";

function App() {
    const {t} = useTranslation();

    useEffect(() => {
        document.title = t("title");
    }, [t]);

    const [subjectId, setSubjectId] = useState(0);
    const [results, setResults] = useState<Results>([]);

    useEffect(() => {
        const resultsStr = localStorage.getItem("results");
        if (resultsStr === null) {
            localStorage.setItem("results", JSON.stringify([]));
            return;
        }

        const results = JSON.parse(resultsStr) as Results;
        setResults(results);
        setSubjectId(results.map(r => r.subjectId).reduce((a, b) => a > b ? a : b, 0));
    }, []);

    useEffect(() => {
        if (results.length > 0) {
            localStorage.setItem("results", JSON.stringify(results));
        }

    }, [results]);

    const [phase, setPhase] = useState<Phase>("MENU");

    const [tests, setTests] = useState<TestSet[]>(testSets);

    const handleStart = useCallback(() => {
        if (phase !== "MENU") return;
        const newSubjectIdStr = prompt(t("promptSubjectId"), `${subjectId + 1}`);
        if (newSubjectIdStr !== null) {
            const newSubjectId = parseInt(newSubjectIdStr);
            if (isNaN(newSubjectId)) {
                alert(t("invalidSubjectId"));
                return;
            }

            setSubjectId(newSubjectId);
            setPhase("INTRO");
        }
    }, [phase, subjectId, t]);

    const handleResults = useCallback(() => {
        if (phase !== "MENU") return;
        setPhase("RESULTS");
    }, [phase])

    const handleEdit = useCallback(() => {
        if (phase !== "MENU") return;
        setPhase("EDIT");
    }, [phase]);

    const handleSaveLoad = useCallback(() => {
        if (phase !== "MENU") return;
        setPhase("LOAD");
    }, [phase]);

    const handleSetTests = (tests: TestSet[]) => {
        setTests(tests);
        setPhase("MENU");
    };

    const handleFinish = (stats: StatLine[]) => {
        setResults([...results, {subjectId, results: stats}]);
        setPhase("MENU");
    };

    return (
        <div>
            {phase === "MENU" &&
                <MenuPanel onStart={handleStart} onResults={handleResults} onEdit={handleEdit} onLoad={handleSaveLoad}/>
            }
            {phase === "INTRO" && <IntroPanel onFinish={() => setPhase("TEST")}/>}
            {phase === "TEST" && <TestPanel tests={tests} onFinish={handleFinish}/>}
            {phase === "RESULTS" && <ResultsPanel results={results} onBack={() => setPhase("MENU")}/>}
            {phase === "EDIT" && <EditPanel testSets={tests} onSave={handleSetTests}/>}
            {phase === "LOAD" && <LoadPanel tests={tests} onLoad={handleSetTests}/>}
        </div>
    );
}

export default App;